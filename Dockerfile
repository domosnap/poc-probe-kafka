FROM openjdk:13-alpine

WORKDIR /opt
COPY target/*.jar ./

ENTRYPOINT ["java", "-jar", "domosnap-probe-kafka-1.0-SNAPSHOT-fat.jar", "-Duser.dir=/tmp"]
