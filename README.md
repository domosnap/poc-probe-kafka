# poc-probe-kafka

## Docker 

- Build 
```docker build -t poc-probe-kafka ./```

- Run
```docker run -p 8081:8081 poc-probe-kafka```
 
## Description
- All event arriving from the OpenWebNet emulator will be push into Kafka event store according the url
of the devices as a key.

## Launch

- First, you need to launch the OpenWebNet emulator from OpenWebNet-Emulator directory
```java -jar bin/felix.jar``` 

- In order to use Kafka as a event store you need to initialize Zookeeper and Kafka.

- Launch Zookeeper
```bin/zookeeper-server-start.sh config/zookeeper.properties```

- Launch Kafka
```bin/kafka-server-start.sh config/server.properties```

- Then you can launch the program.
```java -jar target/domosnap-probe-kafka-1.0-SNAPSHOT-fat.jar```

