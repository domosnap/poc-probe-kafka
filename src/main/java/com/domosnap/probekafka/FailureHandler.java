package com.domosnap.probekafka;

import java.util.logging.Logger;

import io.vertx.core.Handler;
import io.vertx.ext.web.RoutingContext;

public class FailureHandler implements Handler<RoutingContext> {
  
	private static final Logger log = Logger.getLogger(FailureHandler.class.getName());

	
    public void handle(RoutingContext context) {
        Throwable thrown = context.failure();
        recordError(thrown);
        context.next();
    }
  
    private void recordError(Throwable throwable) {
        log.severe(throwable.getMessage());
    }
}
