package com.domosnap.probekafka;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import com.domosnap.engine.Log;
import com.domosnap.engine.event.Event;

public class EventToKafkaConsumer implements Consumer<Event> {

	private static Log log = new Log(EventToKafkaConsumer.class.getSimpleName());

	private KafkaProducer<String, String> producer;
	private String DEFAULT_CONFIG_KAFKA_TOPIC_WRITE = "domosnap-event-topic";

	
	public EventToKafkaConsumer() {
		Map<String, Object> props = new HashMap<String, Object>();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "PLAINTEXT://localhost:9092");
		props.put(ProducerConfig.ACKS_CONFIG, "all"); // message passé à tous mes broker "0"= udp, "1"= tcp = récupérer mais pas stocké. Parametre min.insync.replica = 2
		props.put(ProducerConfig.RETRIES_CONFIG, "0");
		props.put(ProducerConfig.BATCH_SIZE_CONFIG, "100");
		props.put(ProducerConfig.LINGER_MS_CONFIG, "1");
		props.put(ProducerConfig.BUFFER_MEMORY_CONFIG, "33554432");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
				"org.apache.kafka.common.serialization.StringSerializer");

		producer = new KafkaProducer<String, String>(props);
	}
	
	public EventToKafkaConsumer(Map<String, Object> props) {

		producer = new KafkaProducer<String, String>(props);
	}
	
	@Override
	public void accept(final Event event) {

		ProducerRecord<String, String> record = new ProducerRecord<String, String>(DEFAULT_CONFIG_KAFKA_TOPIC_WRITE, event.getCommand().getWhere().getUri(), event.toString());
		producer.send(record, new Callback() {
			
			@Override
			public void onCompletion(RecordMetadata metadata, Exception exception) {
				if (exception != null) {
					log.severe(event.getOrigin(), exception.getMessage());
				} else {
					//log.finest(event.getOrigin(), "Event send to Kafka with success [" + event + "]");
				}
				
			}
		});
	}
	
	@Override
	protected void finalize() throws Throwable {
		producer.close();
	}

}
