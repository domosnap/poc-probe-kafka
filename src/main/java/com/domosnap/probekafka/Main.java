package com.domosnap.probekafka;

import java.util.logging.Logger;

import com.domosnap.engine.adapter.impl.openwebnet.OpenWebNetControllerService;
import com.domosnap.engine.controller.counter.PowerCounter;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.event.EventFactory;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;

public class Main extends AbstractVerticle {

	private static final String APP_JSON = "application/json";
	private static final Logger log = Logger.getLogger(Main.class.getName());
	private static final String SCS_GATEWAY_URL = "scs.gateway.url";
	private static final String PORT = "port";
	private static OpenWebNetControllerService owc;
	private static PowerCounter pc;

	public static void main(String[] args) {
		// To run vertx verticle easily
		Runner.runExample(Main.class);
	}

	@Override
	public void start(Promise<Void> startFuture) throws Exception {
		owc = new OpenWebNetControllerService(config().getString(SCS_GATEWAY_URL, "scs://12345@localhost:1234"));
		// Add the consumer to send event to kafka
		EventFactory.addConsumer(new EventToKafkaConsumer());

		// Create the controller (shadow device)
		vertx.executeBlocking(future -> {
			owc.connect();
			pc = owc.createController(PowerCounter.class, new Where("5101"));
		});

		final Router router = Router.router(vertx);

		// Add service to get probe information with REST
		router.route().failureHandler(new FailureHandler());
		router.get("/probe").produces(APP_JSON).handler(rc -> rc.response().setStatusCode(200)
				.end(pc != null ? String.valueOf(pc.getActivePower()) : "Not connected!"));
		// Healthcheck
		router.get("/health*").handler(HealthCheckHandler.create(vertx).register("health", result -> {
			result.complete(Status.OK());
		}));

		vertx.createHttpServer().requestHandler(router).listen(config().getInteger(PORT, 8082));
		
		log.info("Service probe Started!");
	}

	@Override
	public void stop() throws Exception {
		owc.disconnect();
		pc = null;
		super.stop();
	}
}