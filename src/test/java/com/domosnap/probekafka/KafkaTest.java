package com.domosnap.probekafka;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import com.domosnap.engine.Log;
import com.domosnap.engine.controller.Command;
import com.domosnap.engine.controller.Controller;
import com.domosnap.engine.controller.counter.PowerCounter;
import com.domosnap.engine.controller.what.What;
import com.domosnap.engine.controller.what.impl.StringState;
import com.domosnap.engine.controller.where.Where;
import com.domosnap.engine.event.Event;
import com.domosnap.engine.event.EventJsonCodec;

import net.leadware.kafka.embedded.KafkaSimulator;
import net.leadware.kafka.embedded.properties.BrokerProperties;
import net.leadware.kafka.embedded.properties.ListenerProperties;
import net.leadware.kafka.embedded.properties.SimulatorProperties;
import net.leadware.kafka.embedded.utils.KafkaSimulatorFactory;

public class KafkaTest {
    static KafkaSimulator kafkaSimulator;
    private static final int PORT = 9092;
    private static final String BOOTSTRAP = "PLAINTEXT://127.0.0.1:9092";
    private static final String GROUP_ID = "group-1";
    private static final String TOPIC = "domosnap-event-topic";

    @BeforeAll
    public static void initialize() {
        SimulatorProperties properties = new SimulatorProperties();
        ListenerProperties listener = new ListenerProperties();
        listener.setPort(PORT);

        BrokerProperties broker = new BrokerProperties();
        broker.setListener(listener);

        properties.setBrokerConfigs(List.of(broker));
        KafkaSimulatorFactory factory = new KafkaSimulatorFactory(properties);
        kafkaSimulator = factory.getInstance();
        kafkaSimulator.initialize();
    }

	@RepeatedTest(3)
	@DisplayName("Test connection")
	public void testConnection() throws Throwable {
        assertThat(kafkaSimulator.getPublicBrokersUrls()).isEqualTo(BOOTSTRAP);
	}

    @Test
    public void testMessage() throws Throwable {
        EventToKafkaConsumer consumer = new EventToKafkaConsumer();
        Class<? extends Controller> who = PowerCounter.class;
        What what = new What("device1", new StringState("on"));
        Where where = new Where("5051");
        Event event = new Event(Log.Session.Device, new Command(who, what, where, Command.Type.COMMAND, new PowerCounter()));

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> kConsumer = new KafkaConsumer<>(props);

        Thread thread = new Thread(() -> {
            while(true) {
                consumer.accept(event);
            }
        });
        thread.start();

        kConsumer.subscribe(List.of(TOPIC));

        whileTrue:
        while(true) {
            ConsumerRecords<String, String> records = kConsumer.poll(Duration.ofMillis(100) ); //100
            for(ConsumerRecord<String, String> record : records) {
                assertThat(record.value()).isEqualTo(EventJsonCodec.toJSon(event));
                break whileTrue;
            }
        }
        thread.join(1);
        kConsumer.close();
        
    }

    @AfterAll
    public static void close() {
        kafkaSimulator.destroy();
    }

}