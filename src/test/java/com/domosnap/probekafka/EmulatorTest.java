package com.domosnap.probekafka;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.extension.ExtendWith;

import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import net.leadware.kafka.embedded.KafkaSimulator;
import net.leadware.kafka.embedded.properties.BrokerProperties;
import net.leadware.kafka.embedded.properties.ListenerProperties;
import net.leadware.kafka.embedded.properties.SimulatorProperties;
import net.leadware.kafka.embedded.utils.KafkaSimulatorFactory;


@ExtendWith(VertxExtension.class)
public class EmulatorTest {
    static KafkaSimulator kafkaSimulator;
    private static final int PORT = 9092;
    private static final String BOOTSTRAP = "PLAINTEXT://127.0.0.1:9092";
    private static final String GROUP_ID = "group-1";
    private static final String TOPIC = "domosnap-event-topic";
    private Vertx vertx = Vertx.vertx();
    private String host = "localhost";
	private int port = 8082;
    private static Process openWebRunner;

    @BeforeAll
    public static void initialize(Vertx vertx, VertxTestContext testContext) throws Throwable {
        File directory = new File("./OpenWebNet-Emulator");
        openWebRunner = Runtime.getRuntime().exec("java -jar bin/felix.jar", null, directory);

        SimulatorProperties properties = new SimulatorProperties();
        ListenerProperties listener = new ListenerProperties();
        listener.setPort(PORT);

        BrokerProperties broker = new BrokerProperties();
        broker.setListener(listener);

        properties.setBrokerConfigs(List.of(broker));
        KafkaSimulatorFactory factory = new KafkaSimulatorFactory(properties);
        kafkaSimulator = factory.getInstance();
        kafkaSimulator.initialize();

        // Start server
		vertx.deployVerticle(new Main(), testContext.succeedingThenComplete());
		
		testContext.completeNow();
    }

    @Order(10)
	@RepeatedTest(3)
	@DisplayName("Test connection")
	public void testConnection(VertxTestContext testContext) throws Throwable {
    	assertThat(kafkaSimulator.getPublicBrokersUrls()).isEqualTo(BOOTSTRAP);
        WebClient client = WebClient.create(vertx);

		client
		.get(port, host , "/probe")
		.send(testContext.succeeding(ar -> testContext.verify(() -> {
			assertThat(ar.statusCode()).isEqualTo(200);
			testContext.completeNow();
		})));
	}

    @Order(11)
    @RepeatedTest(3)
    public void testMessage(VertxTestContext testContext) throws Throwable {

        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> kConsumer = new KafkaConsumer<>(props);

        kConsumer.subscribe(List.of(TOPIC));

        whileTrue:
        while(true) {
            ConsumerRecords<String, String> records = kConsumer.poll(Duration.ofMillis(100));
            for(ConsumerRecord<String, String> record : records) {
                break whileTrue;
            }
        }
        kConsumer.close();
        testContext.completeNow();
    }

    @AfterAll
    public static void close() {
        kafkaSimulator.destroy();
    }

}